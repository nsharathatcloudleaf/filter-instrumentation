#! /bin/bash
##
 # Copyright (c) 2016 CloudLeaf. All Rights Reserved.
 #
 # The information contained herein is confidential property of CloudLeaf. The use,
 # copying, transfer or disclosure of such information is prohibited except by express written
 # agreement with CloudLeaf.
 #
 # Author: Jitendra Kulkarni
##
echo "hello"
#tagId="E06089002202"
tagId=$1
echo "time,moving,RSSI" > input.csv
grep $tagId screenlog.0 | grep -v 'RAW\|recv_evt_loop\|Exercising' | grep 'ClfFilterApp:printOnlyEntry input'\
    | sed -e s/ClfFilterApp:printOnlyEntry\ input\ //\
    |sed -e s/\ number//g|sed -e s/\ Tagid/Tagid/| sed -e s/\ /\|/g\
    |awk -F '|' '{print $8,$7,$6}'\
    | sed -e s/dispatchTime:// | sed -e s/\ moving:/,/ | sed -e s/\ rssi:/,/ > input_tmp.csv
cat input_tmp.csv >> input.csv

echo "time,moving,RSSI" > filter_output.csv
grep $tagId screenlog.0 |  grep -v 'RAW\|recv_evt_loop\|Exercising' | grep 'ClfFilterApp:mqttDispatch'\
    | sed -e s/ClfFilterApp:mqttDispatch:\ output\ //\
    |sed -e s/\ number//g|sed -e s/\ Tagid/Tagid/| sed -e s/\ /\|/g\
    | awk -F '|' '{print $8,",,"$6}'\
    | sed -e s/dispatchTime:// | sed -e s/rssi:// | sed -s s/\ /,/ > filter_output_tmp.csv

    grep $tagId screenlog.0 |  grep -v 'RAW\|recv_evt_loop\|Exercising' | grep 'ClfFilterApp:mqttDispatch'\
    | sed -e s/ClfFilterApp:mqttDispatch:\ output\ //\
    |sed -e s/\ number//g|sed -e s/\ Tagid/Tagid/| sed -e s/\ /\|/g\
    | awk -F '|' '{print $8,$7,$6}'\
    | sed -e s/dispatchTime:// | sed -e s/\ moving:/,/ | sed -e s/\ rssi:/,/ >> filter_output.csv

echo "time,moving,RSSI" > mqtt_input.csv
grep $tagId screenlog.0 |  grep -v 'RAW\|recv_evt_loop\|Exercising' | grep 'ClfFilterApp:sendEventToMqttCallback'\
    | sed -e s/ClfFilterApp:sendEventToMqttCallback:\ output\ //\
    |sed -e s/\ number//g|sed -e s/\ Tagid/Tagid/| sed -e s/\ /\|/g\
    | awk -F '|' '{print $8,",,,"$6}'\
    | sed -e s/dispatchTime:// | sed -e s/rssi:// | sed -s s/\ /,/ > mqtt_input_tmp.csv

    grep $tagId screenlog.0 |  grep -v 'RAW\|recv_evt_loop\|Exercising' | grep 'ClfFilterApp:sendEventToMqttCallback'\
    | sed -e s/ClfFilterApp:sendEventToMqttCallback:\ output\ //\
    |sed -e s/\ number//g|sed -e s/\ Tagid/Tagid/| sed -e s/\ /\|/g\
    | awk -F '|' '{print $8,$7,$6}'\
    | sed -e s/dispatchTime:// | sed -e s/\ moving:/,/ | sed -e s/\ rssi:/,/ >> mqtt_input.csv


cat filter_output_tmp.csv >> input_tmp.csv
cat mqtt_input_tmp.csv >> input_tmp.csv

echo "time,moving,input,filter_out,mqtt_in">composite.csv
sort -k 1,1 input_tmp.csv >> composite.csv
rm *_tmp.csv
