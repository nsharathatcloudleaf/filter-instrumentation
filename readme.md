#Repo: filter-instrumentation: Gateway Log Post-processing
This repo has code that allows one to take screenlog.0 file from the gateway and plot the RSSI vs. time
for filter's input, filter's output and mqtt input for a given tag. Authored by Jitendra Kulkarni for Cloudleaf Inc.
##Contents
* *make_csvs.sh:* This is a bash script that reads screenlog.0 and creates RSSI *.vs* time csvs at filter input, filter output and mqtt input. It also creates a composite file to be used for a graphics program such as R.
* *compositePlot.R:* This is an R script that genrates and interactive plot  that can be zoomed-in, out and moused over.
* *sample directory:* This contains a sample input and output files and a picture of the interactive plot created.
##Requiremets
* A gateway running image created from checkin 93b4cdb on develop or later. Log entries must be enabled. Raw logs are not needed.
* bash shell
* If you want to use R, you will need R, Rstudio and R packages lubridate, anytime, reshape2 and plotly
##Usage
* Copy screenlog.0 from the gateway to et1 and then pull it on your local machine from et1.
* Move screenlog.0 to the same directory as the shell script.
* Make sure that the shell script has execute permission, otherwise type *chmod +x make_csvs.sh*. Type *./make_csvs.sh <tagId>*, *e.g*, *./make_csvs.sh E06089002202* 
* You now have a few csvs. If you want to use R, open R-studio. Install packages by typing, *e.g.*, *install_packages("lubridate")* in the R-studio console to install lubridate.
* Set the R-studio working directory to the location of the R script and *source("compositePlot.R")*. You will get a graph that you can interact with. You can press the zoom icon in the R-studion plot viewer (magnifying glass) to pop the graph in a separate window.

